#ifndef SRC_PROTOCOL_H
#define SRC_PROTOCOL_H

#include <QtGlobal>
#include <QVector>
#include <QObject>
#include <QByteArrayList>
#include <QCryptographicHash>

typedef QVector<qint32> VectorInt32;
enum class ControlState : qint32 {NotReady, ReadyForUse};
enum class ProtocolVersion : qint32 {
    ProtocolUnknown    = 0,
    ProtocolVersion_1  = 1,

    ProtocolVersionMax = ProtocolVersion_1 //set to maximum protocol version
};

static QByteArray deriveKey(const QByteArrayList &dataList, const ProtocolVersion protocolVersion = ProtocolVersion::ProtocolVersionMax)
{
    QByteArray result;
    switch (protocolVersion) {
    case ProtocolVersion::ProtocolVersion_1 :
        result = QCryptographicHash::hash(dataList.join(), QCryptographicHash::Sha256);
        break;
    default:
        break;
    }

    return result;
}

//enum class MessagesType : qint32 {ServerDiscover, ServerOffer, HandshakeRequest, HandshakeAck, Error, Command = HandshakeAck + 20, CommandDone, CommandFail, CommandNotFound};

class Command : public QObject
{
    Q_GADGET
public:
    enum Commands : qint32 {
        //Service Group
        ServerDiscover,
        ServerOffer,
        HandshakeRequest,
        HandshakeAck,
        Error,
        CommandDone,
        CommandFail,
        CommandNotFound,
        CommandNeedMoreParametrs,
        CommandTooManyParametrs,
        MaxDataStreamVersion = 20,
        SetDataStreamVersion,

        //Timer Group
        TimerShow            = 30,
        TimerAdd             = 31,
        TimerAbort           = 32,

        //Power Group
        PowerLogoff          = 50,
        PowerLockScreen      = 51,
        PowerReboot          = 52,
        PowerSleep           = 53,
        PowerHibernate       = 54,
        PowerOff             = 58,

        //Media Group
        MediaVolumeDown      = 70,
        MediaVolumeMute      = 71,
        MediaVolumeUp        = 72,
        MediaPlay            = 80,
        MediaStop            = 81,
        MediaPrevious        = 82,
        MediaNext            = 83,
        MediaPlayPause       = 86,
    };
    Q_ENUM(Commands)

//    Command() {}
};

#endif // SRC_PROTOCOL_H

//QByteArray Command::deriveKey(const QByteArrayList &dataList, const Command::Commands protocolVersion)
//{
//    QByteArray result;
//    switch (protocolVersion) {
//    case Command::ProtocolVersion_1 :
//        result = QCryptographicHash::hash(dataList.join(), QCryptographicHash::Sha256);
//        break;
//    default:
//        break;
//    }

//    return result;
//}
