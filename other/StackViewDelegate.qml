import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1

ItemDelegate {
    id: delegate
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    contentItem: RowLayout {
        anchors.fill: parent
        spacing: mainWindow.mainMargin
        Text {}
        Image {
            id: imageId
            fillMode: Image.Pad
            anchors.verticalCenter: parent.verticalCenter
            source: model.image
            sourceSize.height: textId.font.pixelSize
        }
        Text {
            id: textId
            text: model.title
            font.pointSize: 24
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            opacity: !delegate.down ? 0.87 : 1
        }
    }

    highlighted: ListView.isCurrentItem

    background: Rectangle {
        color: !delegate.down ? "#c5cae9" : "#9fa8da"
    }
}
