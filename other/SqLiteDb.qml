import QtQuick 2.5
import QtQuick.LocalStorage 2.0

Item {
//    signal tableChanged(string tableName)
//    property var db
    property string dbName: "SimleRcConf"
    property int majorVersion: 1
    property int minorVersion: 1
    property string description: "Simple Remote Control config database"
    property int estimSize: 1024 * 128

    readonly property string changeVer1_0To1_1: 'PRAGMA foreign_keys=off;
                                        ALTER TABLE server RENAME TO server_old;
                                        CREATE TABLE servers (host TEXT, port INTEGER, last_connect TEXT PRIMARY KEY, passphrase TEXT, connected BOOLEAN DEFAULT 0);
                                        INSERT INTO servers (host, port, last_connect, connected)
                                            SELECT host, port, last_connect, connected FROM server_old;
                                        DROP TABLE server_old;
                                        DROP TABLE param_map;
                                        PRAGMA foreign_keys=on'
    property var arrChangeVer: [ [],
                                 [changeVer1_0To1_1] ]

    function initDb(majorVer, minorVer)
    {
        if (majorVer === undefined) {
            majorVer = majorVersion;
        }
        if (minorVer === undefined) {
            minorVer = minorVersion;
        }

        var db;

        try {
            db = LocalStorage.openDatabaseSync(dbName, majorVer + "." + minorVer, description, estimSize);
            if ( (majorVer === majorVersion) && (minorVer === minorVersion) )  {
                createTables(db);
            } else {
                console.log("change version from " + majorVer + "." + minorVer)
                db.changeVersion(majorVer + "." + minorVer, majorVersion + "." + minorVersion,
                                 function(tx) {
                                     var arr = arrChangeVer[majorVer][minorVer].split(';');
                                     for (var i = 0; i < arr.length; ++i) {
//                                         console.log("arr[i]=", arr[i])
                                         tx.executeSql(arr[i]);
                                     }
                                 }
                                 );
            }
        }
        catch (err) {
            console.log("Ошибка БД", err, "Version " + majorVer + "." + minorVer);
            if (minorVer > 0) {
                initDb(majorVer, minorVer - 1);
            } else if (majorVer > 1) {
                initDb(majorVer - 1, 9);
            }
        }
    }

    function createTables(db)
    {
        if (db === undefined) {
            db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
        }

        db.transaction(
                    function(tx) {
                        // Create the database if it doesn't already exist
                        tx.executeSql('CREATE TABLE IF NOT EXISTS servers (host TEXT, port INTEGER,
                                       last_connect TEXT PRIMARY KEY, passphrase TEXT, connected BOOLEAN DEFAULT 0)');
                    }
                    )        
    }

//    function addServer(host, port)
//    {
//        var db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
//        db.transaction(
//                    function(tx){
//                        tx.executeSql("INSERT INTO servers (host, port) VALUES (?, ?)", [host, port || 38383])
//                    }
//                    )
//    }

    function addRow(table, paramsObject)
    {
        var params = paramsToArray(paramsObject);
        var db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
        db.transaction(
                    function(tx){
                        tx.executeSql("INSERT OR REPLACE INTO " + table + " (" + params[0] + ") VALUES (" + params[1] + ")");
                    }
                    )
//        tableChanged(table);
    }

    function updateRow(table, paramsObject, whereObject)
    {
        var db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
        db.transaction(
                    function(tx){
                        var res = tx.executeSql("UPDATE " + table + " SET " + paramsToString(paramsObject) +
                                      " WHERE " + paramsToString(whereObject, ' AND '));
//                        console.log("UPDATE rowsAffected=", res.rowsAffected);
                    }
                    )
    }

    function readTable(table, whereObject, orderObject)
    {
        var query = "SELECT * FROM " + table + (whereObject === undefined ? "" : " WHERE " + paramsToString(whereObject, ' AND ')) + orderToString(orderObject);
        var arr = [];
        var db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
        db.readTransaction(
                    function(tx){
//                        console.log("query=", query);
                        query = tx.executeSql(query);
                        for(var i = 0; i < query.rows.length; ++i) {
                            arr[i] = query.rows.item(i);
                        }
                    }
                    )
        return arr
    }

    function deleteRow(table, whereObject)
    {
        var db = LocalStorage.openDatabaseSync(dbName, majorVersion + "." + minorVersion, description, estimSize);
        db.transaction(
                    function(tx){
                        tx.executeSql("DELETE FROM " + table + " WHERE " + paramsToString(whereObject, ' AND '));
                    }
                    )
//        tableChanged(table);
    }

    function paramsToArray(paramsObject)
    {

        var parametrNames = [];
        var parametrValues = [];
        for (var name in paramsObject) {
            parametrNames.push(name);
            parametrValues.push(paramsObject[name]);
        }
        return [parametrNames.join(', '), '"' + parametrValues.join('", "') + '"'];
    }

    function paramsToString(paramsObject, separator)
    {
//        console.log("paramsToString", paramsObject, separator)
        if (paramsObject === undefined)
            return "";

        var params = [];
        for (var name in paramsObject) {
            params.push(name + ' = "' + paramsObject[name] + '"');
        }

        return (separator === undefined ? params.join() : params.join(separator));
    }

    function orderToString(paramsObject)
    {
        if (paramsObject === undefined)
            return "";

        var params = [];
        for (var name in paramsObject) {
            params.push(name + (paramsObject[name].toUpperCase() === "DESC" ? " DESC": " ASC"));
        }

        return " ORDER BY " + params.join(", ");
    }

//    Component.onCompleted: {
//        openDb();
//        createTables();
//    }
}

/*
Database Version 1.0
    tx.executeSql('CREATE TABLE IF NOT EXISTS server (host TEXT, port INTEGER,
                                       last_connect TEXT, user TEXT, password TEXT, connected BOOLEAN DEFAULT 0)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS param_map (param TEXT PRIMARY KEY, value TEXT)');
*/
