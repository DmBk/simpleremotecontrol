#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <simpleclient.h>
#include <discoverserver.h>

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("SimpleRC");
    QGuiApplication::setOrganizationName("DmBk");
//    QGuiApplication::setOrganizationDomain("somecompany.com");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<SimpleClient>("SimpleClientSocket", 1, 0, "SimpleClient");
    qmlRegisterType<DiscoverServer>("DiscoverServer", 1, 0, "DiscoverServer");
    qmlRegisterType<Command>("SimpleCommands", 1, 0, "Command");

//    DiscoverServer discoverServer;

    QQmlApplicationEngine engine;
//    engine.rootContext()->setContextProperty("discoverServer", &discoverServer);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
