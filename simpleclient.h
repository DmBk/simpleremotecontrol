#ifndef SIMPLECLIENT_H
#define SIMPLECLIENT_H

#include <QObject>
#include "clientsocket.h"
#include "src_protocol.h"
#include <QDataStream>
#include <QSslError>
#include <QByteArray>

class SimpleClient : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QByteArray passPhrase READ passPhrase WRITE setPassPhrase NOTIFY passPhraseChanged)
    Q_PROPERTY(QByteArray clientIdentity READ clientIdentity WRITE setClientIdentity NOTIFY clientIdentityChanged)
public:
    explicit SimpleClient(QObject *parent = Q_NULLPTR);
    Q_INVOKABLE void connectToHost(const QString &host, quint16 port = 38383, int timeout = 0);
    Q_INVOKABLE void sendCommand(const int idCommand, const QString str = QString());
    Q_INVOKABLE void sendCommand(const QVector<int> vector, const QString str = QString());
    Q_INVOKABLE bool clientReady() const;
    QByteArray passPhrase() const;
    void setPassPhrase(const QByteArray &psk);
    QByteArray clientIdentity() const;
    void setClientIdentity(const QByteArray &clientIdentity);

signals:
    void timeoutConection();
    void handshakeSuccess();
    void passPhraseChanged();
    void clientIdentityChanged();
    void commandResult(int result);

protected slots:
    void onDeliveredData(VectorInt32 vector, QByteArray data);
    void handshake();
    void onSocketError(QAbstractSocket::SocketError &errors);
    void onSslError(const QList<QSslError> &errorList);
    void onSocketStateChanged(QAbstractSocket::SocketState socketState);
    void onTimeout();
    void closeConnection();
//    void closeConnection(const QString &host);
    void onDestroyed();
    void onPreSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *authenticator);
//    void onHandshakeSuccess();
private:    
    ClientSocket *m_socket;
    QByteArray m_passPhrase;
    QByteArray m_identity;
    ProtocolVersion m_protocol {ProtocolVersion::ProtocolUnknown};
};

#endif // SIMPLECLIENT_H
