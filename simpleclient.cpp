#include "simpleclient.h"

#include <QHostAddress>
#include <QSslConfiguration>
#include <QSslCipher>
//#include <QSslKey>
//#include <QSslCertificate>
//#include <QFile>
#include <QTimer>
#include <QDebug>
#include <QTime>
#include <QSslPreSharedKeyAuthenticator>
#include <QByteArray>
#include <QByteArrayList>


SimpleClient::SimpleClient(QObject *parent) : QObject(parent), m_socket(Q_NULLPTR)
{

}

void SimpleClient::connectToHost(const QString &host, quint16 port, int timeout)
{    
    if (m_socket != Q_NULLPTR) {
        closeConnection();
    }

    m_socket = new ClientSocket(this);
//    qDebug() << m_socket << "host" << host;
    m_socket->setReadYourself();
//    connect(m_socket, &QSslSocket::disconnected, [=](){ qDebug() << "sslErrors disconnected" << m_socket->sslErrors() << m_socket->error()
//                                                                 << m_socket->errorString(); });
//    connect(m_socket, static_cast<void(QSslSocket::*)(const QList<QSslError> &)>(&QSslSocket::sslErrors),
//            [=](const QList<QSslError> &errors){qDebug() << QTime::currentTime() << "sslError lambda"; qDebug() << errors;});
//    connect(m_socket, &QAbstractSocket::disconnected, m_socket, &QAbstractSocket::deleteLater);
    connect(m_socket, &QAbstractSocket::disconnected, this, &SimpleClient::closeConnection);
    connect(m_socket, &ClientSocket::dataReaded, this, &SimpleClient::onDeliveredData);
//    connect(m_socket, static_cast<void(QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
//            this, &SimpleClient::onSocketError);
//    connect(m_socket, &QAbstractSocket::stateChanged, this, &SimpleClient::onSocketStateChanged);

    connect(m_socket, &QAbstractSocket::connected, this, &SimpleClient::handshake);
    m_socket->connectToHost(host, port);

    if (timeout > 0) {
        QTimer::singleShot(timeout, this, &SimpleClient::onTimeout);
    }
}

void SimpleClient::sendCommand(const int idCommand, const QString str)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << idCommand << str;
    QVector<int> vec{idCommand};
    sendCommand(vec, str);
}

void SimpleClient::sendCommand(const QVector<int> vector, const QString str)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << vector[0] << str;
    VectorInt32 serializeVector;
    for (int i = 0; i < vector.size(); ++i) {
        serializeVector.push_back(static_cast<qint32>(vector[i]));
    }

    if (m_socket != Q_NULLPTR && m_socket->clientState() == ControlState::ReadyForUse)
        m_socket->writeToSocket(serializeVector, str.toUtf8());
}

bool SimpleClient::clientReady() const
{
    return ((m_socket != Q_NULLPTR) && (m_socket->clientState() == ControlState::ReadyForUse));
}

QByteArray SimpleClient::passPhrase() const
{
    return m_passPhrase;
}

void SimpleClient::setPassPhrase(const QByteArray &psk)
{
    QSslConfiguration sslConfig = QSslConfiguration::defaultConfiguration();
//    qDebug() << sslConfig.ciphers() << sslConfig.privateKey() << sslConfig.protocol();
    QList<QSslCipher> ciphersList;
    ciphersList << QSslCipher("PSK-AES256-CBC-SHA");
    sslConfig.setCiphers(ciphersList);
    sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
    QSslConfiguration::setDefaultConfiguration(sslConfig);

    if (m_passPhrase != psk) {
        m_passPhrase = psk;
        emit passPhraseChanged();
    }
}

QByteArray SimpleClient::clientIdentity() const
{
    return m_identity;
}

void SimpleClient::setClientIdentity(const QByteArray &clientIdentity)
{
    if (m_identity != clientIdentity) {
        m_identity = clientIdentity;
        emit clientIdentityChanged();
    }
}

void SimpleClient::closeConnection()
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << 1;
    if (m_socket != Q_NULLPTR) {
        //for remove loop call closeConnection
        disconnect(m_socket, &QAbstractSocket::disconnected, this, &SimpleClient::closeConnection);

        //    m_socket->disconnect();
        m_socket->abort();
        emit timeoutConection();
        qDebug() << QTime::currentTime() << __FUNCTION__ << 2;
//        delete m_socket;
        m_socket->deleteLater();
        qDebug() << QTime::currentTime() << __FUNCTION__ << 3;
        m_socket = Q_NULLPTR;
        qDebug() << m_socket;
    }
}

void SimpleClient::onDeliveredData(VectorInt32 vector, QByteArray data)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << vector << QString::fromUtf8(data);

    switch (m_socket->clientState()) {
    case ControlState::ReadyForUse :
        qDebug() << QTime::currentTime() << "ControlState::ReadyForUse";
        emit commandResult(static_cast<int>(vector[0]));
        break;
    case ControlState::NotReady :
        qDebug() << QTime::currentTime() << "ControlState::Handshake" << vector;
        if ((vector.size() > 1)
                && (vector[0] == static_cast<qint32>(Command::HandshakeAck))
                && (vector[1] <= static_cast<qint32>(ProtocolVersion::ProtocolVersionMax))
                && (vector[1] >= static_cast<qint32>(ProtocolVersion::ProtocolVersion_1))) {
            if (!m_passPhrase.isEmpty()) {
                connect(m_socket, &QSslSocket::encrypted, [=](){ m_socket->setClientState(ControlState::ReadyForUse);
                                                                 emit handshakeSuccess();
                                                                });
                connect(m_socket, &QSslSocket::preSharedKeyAuthenticationRequired, this, &SimpleClient::onPreSharedKeyAuthenticationRequired);
                connect(m_socket, SIGNAL(sslErrors(const QList<QSslError>&)), this, SLOT(onSslError(const QList<QSslError>&)));
                m_protocol = static_cast<ProtocolVersion>(vector[1]);
                m_socket->startClientEncryption();
            } else {
                m_protocol = static_cast<ProtocolVersion>(vector[1]);
                m_socket->setClientState(ControlState::ReadyForUse);
                emit handshakeSuccess();
            }
        } else {
            closeConnection();
            qDebug() << QTime::currentTime() << "Not ReadyForUse";
        }
        break;    
    default:
        qDebug() << QTime::currentTime() << "default";
        break;
    }
}

void SimpleClient::handshake()
{    
    qDebug() << QTime::currentTime() << __FUNCTION__;
//    m_socket->setClientState(ControlState::Handshake);
    VectorInt32 vector {static_cast<qint32>(Command::HandshakeRequest)};
    vector.push_back(static_cast<qint32>(ProtocolVersion::ProtocolVersionMax));
    m_socket->writeToSocket(vector, QByteArray());
}

void SimpleClient::onSocketError(QAbstractSocket::SocketError &errors)
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    switch (errors) {
    case QAbstractSocket::RemoteHostClosedError:
        qDebug() << "Remote Host Disconnected";
        emit timeoutConection();
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << tr("TcpClient") <<
                    tr("The host was not found. Please check the "
                       "host name and port settings.");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << tr("TcpClient") <<
                    tr("The connection was refused by the peer. "
                       "Make sure the simple server is running, "
                       "and check that the host name and port "
                       "settings are correct.");
        break;
    case QAbstractSocket::SocketTimeoutError:
        qDebug() << tr("TcpClient") <<
                    tr("The connection was timeout error. "
                       "Make sure the simple server is running, "
                       "and check that the host name and port "
                       "settings are correct.");
        break;
    default:
        qDebug() << tr("TcpClient") <<
                    tr("The following error occurred: %1.").arg(m_socket->errorString());
    }
}

void SimpleClient::onSslError(const QList<QSslError> &errorList)
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    qDebug() << errorList;
}

void SimpleClient::onSocketStateChanged(QAbstractSocket::SocketState socketState)
{
    qDebug() << socketState << m_socket->error() << m_socket->errorString();
}

void SimpleClient::onTimeout()
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << "m_socket=" << m_socket;
    //if user close connection before timeout triggered m_socket is NULL
    if (m_socket != Q_NULLPTR
            && (m_socket->state() != QAbstractSocket::ConnectedState || m_socket->clientState() != ControlState::ReadyForUse)) {
        qDebug() << __FUNCTION__ << "onTimeout != QAbstractSocket::ConnectedState" << m_socket->state();
        emit timeoutConection();
    }
}

void SimpleClient::onDestroyed()
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << m_socket;
}

void SimpleClient::onPreSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *authenticator)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << sender();
//    qDebug() << "authenticator->identity() " << authenticator->identity() <<
//                "authenticator->identityHint() " << authenticator->identityHint() <<
//                "authenticator->preSharedKey() " << authenticator->preSharedKey() <<
//                "authenticator " << authenticator;
    authenticator->setIdentity(m_identity + QTime::currentTime().toString(" HH:mm:ss.zzz").toUtf8());
    QByteArrayList dataList;
    dataList << m_passPhrase << authenticator->identityHint() << authenticator->identity();
    authenticator->setPreSharedKey(deriveKey(dataList, ProtocolVersion::ProtocolVersion_1));
    qDebug() << "authenticator->identity() " << authenticator->identity() <<
                "authenticator->identityHint() " << authenticator->identityHint() <<
                "authenticator->preSharedKey() " << authenticator->preSharedKey() <<
                "authenticator " << authenticator ;
//    qDebug() << "authenticator->identity() " << authenticator->identity() <<
//                "authenticator->identityHint() " << authenticator->identityHint() <<
//                "authenticator->preSharedKey() " << authenticator->preSharedKey() <<
//                "authenticator " << authenticator << "authenticator->maximumPreSharedKeyLength() " << authenticator->maximumPreSharedKeyLength() <<
    //                "authenticator->maximumIdentityLength() " << authenticator->maximumIdentityLength();
}

//void SimpleClient::onHandshakeSuccess()
//{
//    disconnect(m_socket, &ClientSocket::dataReaded, this, &SimpleClient::onDeliveredData);
//    connect(m_socket, &ClientSocket::dataReaded, this, &SimpleClient::commandResult);
//    m_socket->setClientState(ControlState::ReadyForUse);
//    emit handshakeSuccess();
//}
