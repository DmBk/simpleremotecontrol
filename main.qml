import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtQml 2.2
import SimpleClientSocket 1.0
import SimpleCommands 1.0
import QtQuick.Controls.Material 2.1
import "other"

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Пульт управления компьютером")

    signal connectToServer(int index)
    signal closeServerConnection()

    property alias simpleClient: simpleClient
    property alias serverModel: serverModel
    property alias stackView: stackView
    property alias config: config
    property string toolBarTitle: qsTr("Управление компьютером")

    readonly property string serverTable: "servers"
    readonly property var arrServerRoles: ["host", "port", "last_connect", "connected", "passphrase"]

    property int lastConnectModelIndex: -1
    property bool serverConnected: false
    property int defaultPort: 38383

    property int maxDepthStackViev: 5
    property int mainMargin: 5

    SqLiteDb {
        id: config
    }

    ListModel {
        id: serverModel

        function readData()
        {
            var table = config.readTable(serverTable, undefined, {"last_connect":"DESC"})
            for (var i = 0; i < table.length; ++i) {
                serverModel.append(table[i])
            }
        }

        function objectWithRoles(index, arrRoles)
        {
            var obj = {}
            if (arrRoles === undefined)
                arrRoles = arrServerRoles
            for (var i = 0; i < arrRoles.length; ++i) {
                var role = arrRoles[i]
                if (serverModel.get(index)[role] !== undefined)
                    obj[role] = serverModel.get(index)[role]
            }
            return obj
        }
    }

    header: ToolBar {
        id: toolBar

        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                contentItem: Image {
                    fillMode: Image.Pad
                    horizontalAlignment: Image.AlignHCenter
                    verticalAlignment: Image.AlignVCenter
                    source: "qrc:/images/drawer@2x.png"
                }
                onClicked: drawer.open()
            }

            Label {
                id: titleLabel
                text: toolBarTitle
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                contentItem: Image {
                    fillMode: Image.Pad
                    horizontalAlignment: Image.AlignHCenter
                    verticalAlignment: Image.AlignVCenter
                    source: "qrc:/images/menu@2x.png"
                }
                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: qsTr("О программе")
                        opacity: 0.87
                        onTriggered: aboutDialog.open()
                    }
                }
            }
        }
    }

    ListModel {
        id: menuModel
        ListElement { title: QT_TR_NOOP("Медиа"); source: "qrc:/pages/MediaPage.qml"; image: "qrc:/images/media.png"}
        ListElement { title: QT_TR_NOOP("Питание"); source: "qrc:/pages/PowerControl.qml"; image: "qrc:/images/color-power.png"}
        ListElement { title: QT_TR_NOOP("Выбор сервера"); source: "qrc:/pages/Settings.qml"; image: "qrc:/images/servers.png"}
    }

    Drawer {
        id: drawer
        width: Math.min(mainWindow.width, mainWindow.height) / 3 * 2
        height: mainWindow.height

        ListView {
            id: listView
            currentIndex: -1
            anchors.fill: parent
            spacing: mainMargin

            header: Rectangle {
                width: parent.width
                height: toolBar.height
                color: Material.primary
                Label {
                    anchors.centerIn: parent
                    Material.foreground: "white"
                    text: textInfo.text
                    anchors.bottomMargin: mainMargin
                }
                anchors.bottomMargin: mainMargin
            }

            delegate: StackViewDelegate {

                onClicked: {
                    if (listView.currentIndex !== index) {
                        listView.currentIndex = index
                        titleLabel.text = model.title
                        if (stackView.depth > maxDepthStackViev) {
                            stackView.pop(StackView.Immediate)
                        }
                        stackView.push(model.source)
                    }
                    drawer.close()
                    stackView.focus = true
                }

                background: Rectangle {
                    id: backgroundRect
                    color: !down ? "#e8eaf6" : "#c5cae9"
                }
            }

            model: menuModel

            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }

    StackView {
        id: stackView
        //        anchors.fill: parent
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: textInfo.top
            topMargin: mainMargin
        }
        initialItem: ListView {
            model: menuModel
            spacing: mainMargin
            delegate: StackViewDelegate {
                width: parent.width - 10
                onClicked: {
                        listView.currentIndex = index
                        titleLabel.text = model.title
                        stackView.push(model.source)
                }
            }
        }

        Keys.onBackPressed: {
            console.log("Back button")
            if (stackView.depth > 1) {
                stackView.pop()
                event.accepted = true
            } else {
                event.accepted = false
            }
        }
    }

    SimpleClient {
        id: simpleClient
        clientIdentity: "Simple RC Client"

        onTimeoutConection: {
            //            textInfo.visible = true
            //            textInfo.height = 20
            textInfo.text = qsTr("Ошибка соединения с сервером")
            textInfo.color = Material.accent
            serverConnected = false
        }

        onHandshakeSuccess: {
            var dt = new Date()
            dt = dt.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss")
            config.updateRow(serverTable, {"connected":1, "last_connect":dt}, serverModel.objectWithRoles(lastConnectModelIndex))
            serverModel.setProperty(lastConnectModelIndex, "connected", 1)
            serverModel.setProperty(lastConnectModelIndex, "last_connect", dt)
            //            textInfo.visible = true
            textInfo.text = qsTr("Подключен к серверу ") + serverModel.get(lastConnectModelIndex).host
            textInfo.color = Material.primary
            //            textInfo.height = 0
            serverConnected = true
        }

        onCommandResult: {
            switch (result) {
            case Command.CommandDone:
                labelCommandResult.text = qsTr("Успешно")
                break
            case Command.CommandFail:
            case Command.CommandNeedMoreParametrs:
            case Command.CommandTooManyParametrs:
                labelCommandResult.text = qsTr("Команда не выполнена")
                break
            case Command.CommandNotFound:
                labelCommandResult.text = qsTr("Команда не известна серверу")
                break
            default:
                labelCommandResult.text = qsTr("Неизвестный результат")
            }

            labelCommandResult.visible = true
            animationCommandResult.restart()
        }
    }

    Popup {
        id: aboutDialog
        modal: true
        focus: true
        width: Math.min(mainWindow.width, mainWindow.height) / 3 * 2
        x: (mainWindow.width - width) / 2
        y: (mainWindow.height - height) / 4
        contentItem: Column {
            id: aboutColumn
            spacing: 20

            Label {
                text: "About"
                font.bold: true
            }

            Label {
                width: aboutDialog.availableWidth
                text: qsTr("Простой пульт управления компьютером")
                wrapMode: Label.Wrap
                font.pixelSize: 18
            }

            Label {
                width: aboutDialog.availableWidth
                text: qsTr("Позволяет выполнять простые операции с компьютером не подходя к нему")
                wrapMode: Label.Wrap
                font.pixelSize: 14
            }

            Label {
                text: qsTr("Разработчик: Дмитрий Бибик")
            }
            Label {
                text: "email: dm.mailbox1@gmail.com"
            }
        }
    }


    Label {
        id: textInfo
        text: qsTr("Нет соединения")
        anchors {
            //            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: 5
        }
        font.pointSize: 14
        color: Material.primary
        //        height: 0
        horizontalAlignment: Text.AlignHCenter
        z: 10
    }

    Label {
        id: labelCommandResult
        visible: false
        anchors {
            top: parent.top
            margins: mainMargin
            horizontalCenter: parent.horizontalCenter
        }
        leftPadding: 10
        rightPadding: 10
        topPadding: 3
        bottomPadding: 3
        font.pointSize: 18
        horizontalAlignment: Text.AlignHCenter
        color: Material.accent
        background: Rectangle {
            radius: 6
            color: "#c5cae9"
            opacity: 0.5
        }
        z: 10        
    }

    SequentialAnimation {
        id: animationCommandResult
//        running: labelCommandResult.visible
        PropertyAnimation { target: labelCommandResult; property: "opacity"; from: 1; to: 0; duration: 4000 }
        PropertyAnimation { target: labelCommandResult; property: "visible"; to: false}
    }

    Timer {
        id: timerCommandResult
        interval: 3000
        running: labelCommandResult.visible
        repeat: false
        triggeredOnStart: false
        onTriggered: {
            labelCommandResult.visible = false
        }
    }

    Timer {
        id: timerConnected
        interval: 1000
        running: !serverConnected && mainWindow.active && lastConnectModelIndex >= 0
        triggeredOnStart: false
        repeat: true
        onTriggered: {            
            console.log("timer onTriggered lastConnectModelIndex=", lastConnectModelIndex, "interval=", interval)
            connectToServer((lastConnectModelIndex >= 0 ? lastConnectModelIndex : 0))
            if (interval < 300000) {
                interval += 15000
            }
        }
    }

    onConnectToServer: {
        simpleClient.passPhrase = serverModel.get(index).passphrase || "";
        simpleClient.connectToHost(serverModel.get(index).host, serverModel.get(index).port, 5000);
        lastConnectModelIndex = index;
        console.log(index, serverModel.get(index).host, serverModel.get(index).port,
                    serverModel.get(index).last_connect)
    }

    onCloseServerConnection: {
        simpleClient.closeConnection()
        textInfo.color = Material.primary
        textInfo.text = qsTr("Нет соединения");
        config.updateRow(serverTable, {"connected":0}, serverModel.objectWithRoles(lastConnectModelIndex))
        serverModel.setProperty(lastConnectModelIndex, "connected", 0)
        lastConnectModelIndex = -1
    }

    Component.onCompleted: {
        config.initDb();
        serverModel.readData();

        if ((serverModel.count > 0) && (serverModel.get(0).connected === 1)) {
            connectToServer(0);
        }
    }
}
