#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

//#include <QObject>
#include <QtNetwork/QSslSocket>
#include <QDataStream>
#include "src_protocol.h"

class ClientSocket : public QSslSocket
{
    Q_OBJECT
public:
    ClientSocket(QObject *parent = Q_NULLPTR);
    ClientSocket(qintptr socketDescriptor, QObject *parent = Q_NULLPTR);
    ~ClientSocket();
    QDataStream::Version dataStreamVersion() const;
    QDataStream::Version maxDataStreamVersion() const;
    void setDataStreamVersion(const QDataStream::Version ver);
    void setReadYourself(bool readYourself = true);
    void writeToSocket(const QByteArray &data);
    void writeToSocket(const VectorInt32 &vector, const QByteArray &data = QByteArray());
    void setClientState(const ControlState &state);
    ControlState clientState() const;
signals:
    void dataReaded(VectorInt32 vector, QByteArray data);
    void clientStateChanged(ControlState state);
public slots:    
protected slots:
    void readFromSocket();
//    void onConnected();
private:
    QDataStream::Version m_dataStreamVersion;
    QDataStream::Version m_maxDataStreamVersion;
    ControlState m_clientState;
};

#endif // CLIENTSOCKET_H
