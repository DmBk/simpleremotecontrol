#ifndef DISCOVERSERVER_H
#define DISCOVERSERVER_H

#include <QObject>
//#include <QNetworkConfigurationManager>
#include <QNetworkInterface>
#include <QUdpSocket>
#include "src_protocol.h"
#include <QDataStream>


class DiscoverServer : public QObject
{
    Q_OBJECT
public:
    explicit DiscoverServer(QObject *parent = 0);
//    QPair<quint32, quint32> rangeLocalIp() const;
    QList<QNetworkAddressEntry> addressEntries() const;
    Q_INVOKABLE void scanLocalnet();
    bool ipInLocalSubnet(QHostAddress ipAddress) const;
//    void scanSubnet(int beginIp, int endIp);
    void broadcastScan(QNetworkAddressEntry addressEntry, quint16 port = 38383);


signals:
    void serverDiscovered(QString serverAddr, int serverPort);

public slots:
    void onUdpReadyRead();
private:
    QUdpSocket m_udpSocket;
};

#endif // DISCOVERSERVER_H
