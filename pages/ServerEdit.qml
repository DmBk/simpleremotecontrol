import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ColumnLayout {
    spacing: 10
    property alias serverName: serverName
    property alias serverPassphrase: serverPassphrase
    property alias serverPort: serverPort
    property alias buttonOk: buttonOk
    property alias buttonCancel: buttonCancel
    //    property alias popupName: popupName

    //    Label {
    //        id: popupName
    //        text: qsTr("Добавить новый сервер")
    //        font.underline: true
    //        font.bold: true
    //        //        Layout.fillWidth: true
    //        font.pointSize: 12
    //        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
    //        horizontalAlignment: Text.AlignHCenter
    //        wrapMode: Text.WordWrap
    //    }

    Label {
        text: qsTr("Сервер(имя или IP адрес)")
    }

    TextField {
        id: serverName
        Layout.fillWidth: true
        Layout.fillHeight: false
    }

    Label {
        text: qsTr("Пароль")
    }

    TextField {
        id: serverPassphrase
        Layout.fillWidth: true
        Layout.fillHeight: false
    }

    Label {
        text: qsTr("Порт")
    }

    SpinBox {
        id: serverPort
        Layout.fillHeight: false
        Layout.fillWidth: false
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        from: 1
        value: 38383
        to: 65535
        editable: true
    }

    RowLayout {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        spacing: 10

        Button {
            id: buttonOk
            text: "Ok"
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: 0
            Layout.fillWidth: true
        }

        Button {
            id: buttonCancel
            text: qsTr("Отмена")
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: 0
            Layout.fillWidth: true
        }
    }
}
