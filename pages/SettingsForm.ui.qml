import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Pane {
    //    width: 400
    //    height: 540
    //    height: columnLayout1.childrenRect.height + columnLayout1.children.size() * columnLayout1.spacing
    //    height: columnLayout1.height + columnLayout1.spacing * 15
    //    anchors.fill: parent
    property alias spinBoxPort: spinBoxPort
    property alias listAvailabelServers: listAvailabelServers
    property alias listSavedServers: listSavedServers

    ColumnLayout {
        id: columnLayout1
        spacing: 5
        //        width: 293
        //        height: implicitHeight + spacing * 16
        //        anchors.fill: parent

        Label {
            id: label1
            text: qsTr("Серверы")
            font.bold: true
        }

        ListView {
            id: listSavedServers
            width: 100
            height: 100
        }

        Label {
            id: label2
            text: qsTr("Доступные серверы")
            font.bold: true
        }

        ListView {
            id: listAvailabelServers
            width: 100
            height: 100
        }

        Label {
            id: label3
            x: 6
            y: 240
            text: qsTr("Порт по умолчанию")
            font.bold: true
        }

        SpinBox {
            id: spinBoxPort
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            from: 1
//            value: 38383
            to: 65535
            editable: true
        }

    }

}
