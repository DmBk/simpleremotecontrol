import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import SimpleCommands 1.0
import QtQuick.Controls.Material 2.1

PowerControlForm {
    id: powerControl
    flickableDirection: Flickable.AutoFlickIfNeeded
    contentHeight: gridLayout.height
    implicitHeight: mainWindow.stackView.height
    ScrollIndicator.vertical: ScrollIndicator {}
    columns: (Screen.height > Screen.width ? 2 : 4)
    property int rows: 8 / columns
    property real buttonWidth: (powerControl.width - (columns + 1) * 5) / columns    
    property real buttonHeight: powerControl.height / rows
//    property real buttonHeight: buttonWidth / 3 * 2
//    Material.background: Qt.lighter(Material.primary)
//    Material.primary: Material.Pink
    backgroundColor: "#303f9f"
//    Material.background: Material.Red
    Material.foreground: "white"
//    Material.foreground: Material.accent


    buttonHibernate {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerHibernate, buttonHibernate.text)
        }
    }

    buttonLock {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerLockScreen, buttonLock.text)
        }
    }

    buttonLogOff {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerLogoff, buttonLogOff.text)
        }
    }

    buttonPowerOff {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerOff, buttonPowerOff.text)
        }
    }

    buttonRestart {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerReboot, buttonRestart.text)
        }
    }

    buttonSleep {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            runCommand(Command.PowerSleep, buttonSleep.text)
        }
    }

    buttonTimer {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            editTimer.open()
        }
    }

    buttonAbortTimer {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        onClicked: {
            spinBoxTimer.value = editTimer.timeValue = 0
            buttonTimer.text = qsTr("Таймер")
            runCommand(Command.TimerAbort)
        }
    }

    Popup {
        id: editTimer
        property int timeValue
        x: (mainWindow.width - width) / 2
        y: (mainWindow.height - height) / 3
        implicitWidth: Math.min(parent.height, parent.width) / 3 * 2
        modal: true
        focus: true
        contentItem: ColumnLayout {
            spacing: 10
            RowLayout {
                Layout.fillWidth: true
                Button {
                    text: "5"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }
                Button {
                    text: "15"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }
                Button {
                    text: "30"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }

            }
            RowLayout {
                Layout.fillWidth: true
                Button {
                    text: "45"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }
                Button {
                    text: "60"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }
                Button {
                    text: "90"
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = spinBoxTimer.valueFromText(text)
                    }
                }
            }
            Label {
                text: qsTr("Задержка времени (минут)")
            }
            SpinBox {
                id: spinBoxTimer
                from: 0
                value: 0
                to: (Math.pow(2,31) - 1) / 1000 / 60  //MAX int32 in milliseconds to minutes
                editable: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                valueFromText: function(text) { return Number.fromLocaleString(Qt.locale(), text); }
            }
            RowLayout {
                Layout.fillWidth: true
//                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                spacing: 10

                Button {
                    id: buttonOk
                    text: qsTr("Ok")
                    Layout.fillHeight: false
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.preferredWidth: 0
                    Layout.fillWidth: true
                    onClicked: {
                        editTimer.timeValue = spinBoxTimer.value
                        editTimer.close()
                        buttonTimer.text = (editTimer.timeValue == 0 ? qsTr("Таймер") :
                                                qsTr("Задержка:\n") + spinBoxTimer.value.toLocaleString(Qt.locale(), 'f', 0)
                                                                       + "\n" + qsTr("мин"))
                    }
                }

                Button {
                    id: buttonCancel
                    text: qsTr("Отмена")
                    Layout.fillHeight: false
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.preferredWidth: 0
                    Layout.fillWidth: true
                    onClicked: {
                        spinBoxTimer.value = editTimer.timeValue
                        editTimer.close()
                    }
                }
            }
        }

        Component.onCompleted: {
            timeValue = spinBoxTimer.value
        }

        onOpened: {
            timeValue = spinBoxTimer.value
        }
    }

    function runCommand(commandId, comment)
    {
//        console.log("editTimer.timeValue", editTimer.timeValue)
        if (editTimer.timeValue == 0) {
            mainWindow.simpleClient.sendCommand(commandId)
        } else {
            var arr = []
            arr.push(Command.TimerAdd, editTimer.timeValue * 60 * 1000, commandId)
            mainWindow.simpleClient.sendCommand(arr, comment || "")
//            console.log("arr", arr, arr.length, arr[0], arr[1], arr[2])
        }
    }

//    ColumnLayout {
//        anchors.fill: parent
//        Rectangle {
//            color: Material.accent
////                Layout.fillWidth: true
//            width: 80
//            height: 80
//        }
//        Rectangle {
//            color: Material.background
//            width: 80
//            height: 80
//        }
//        Rectangle {
//            color: Material.foreground
//            width: 80
//            height: 80
//        }
//        Rectangle {
//            color: Material.primary
//            width: 80
//            height: 80
//        }
//        Rectangle {
//            color: Material.color(Material.Teal)
//            width: 80
//            height: 80
//        }
//    }
}
