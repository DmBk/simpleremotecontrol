import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Window 2.2
import SimpleCommands 1.0
import QtQuick.Controls.Material 2.1

MediaPageForm {
    id: mediaPage
    flickableDirection: Flickable.AutoFlickIfNeeded
    contentHeight: gridLayoutMedia.height
    ScrollIndicator.vertical: ScrollIndicator {}
    columns: (Screen.height > Screen.width ? 2 : 4)
    property int rows: 8 / columns
    property real buttonWidth: (mediaPage.width - 2 * 12 - columns * 5) / columns
    property real buttonHeight: mediaPage.height / rows
//    Material.background: Qt.lighter(Material.primary)
//    backgroundColor: Material.background
    backgroundColor: "#303f9f"
    Material.foreground: "white"


    buttonVolDown {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-voldown.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaVolumeDown)
        }
    }
    buttonVolMute {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-mute.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaVolumeMute)
        }
    }
    buttonVolUp {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-volup.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaVolumeUp)
        }
    }
    buttonMediaPlay {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-play.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaPlay)
        }
    }
    buttonMediaStop {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-stop.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaStop)
        }
    }
    buttonMediaPrevious {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-back.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaPrevious)
        }
    }
    buttonMediaNext {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-next.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaNext)
        }
    }
    buttonMediaPlayPause {
        implicitWidth: buttonWidth
        implicitHeight: buttonHeight
        contentItem:  Image {
            fillMode: Image.Pad
            source: "../images/media-playpause.png"
            sourceSize.height: parent.height / 5
        }
        onClicked: {
            mainWindow.simpleClient.sendCommand(Command.MediaPlayPause)
        }
    }
    onWidthChanged: {
        columns = (Screen.height > Screen.width ? 2 : 4)
        console.log(Screen.width, Screen.height)
    }    
}
