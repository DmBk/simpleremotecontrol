import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQml 2.2
import QtQuick.Controls.Material 2.0
import DiscoverServer 1.0

Flickable {
    id: flickable
    property int minHeight: 10
    property int buttonHeight: 40
    property int indexConnectedServer
    flickableDirection: Flickable.AutoFlickIfNeeded
    contentHeight: settingsForm.height


    Component {
        id: serverDelegate
        ItemDelegate {
            id: control
            spacing: mainWindow.mainMargin
            width: flickable.width - 20
            height: buttonHeight
            contentItem: RowLayout {
                anchors.fill: parent
                spacing: parent.spacing
                anchors.margins: 0
                ColumnLayout {
                    spacing: parent.spacing
                    Layout.fillWidth: true
                    Layout.preferredWidth: 250
                    anchors {
                        verticalCenter: parent.verticalCenter
                    }
                    Text {
                        leftPadding: parent.spacing
                        opacity: !control.down ? 0.87 : 1
                        text: model.host + ":" + model.port
                        font: control.font
                        color: "black"
                        elide: Text.ElideRight
                        //                        Layout.fillWidth: true
                    }
                    Text {
                        leftPadding: parent.spacing
                        opacity: !control.down ? 0.54 : 0.87
                        text: model.last_connect || qsTr("Never")
                        color: "black"
                        elide: Text.ElideRight
                        //                        Layout.fillWidth: true
                    }
                }
                RowLayout {//for right alignment
                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                        margins: 0
                    }
                    Button {
                        text: qsTr("Edit")
                        Layout.fillHeight: true
                        implicitWidth: height
                        anchors {
                            verticalCenter: parent.verticalCenter
                            margins: 0
                        }
                        contentItem:  Image {
                            fillMode: Image.PreserveAspectFit
                            source: "../images/edit.png"
                        }
                        onClicked: {
                            serverEdit.addServerWindow = false
                            serverEdit.modelIndex = index
                            addServer(model.host, model.port, model.passphrase)
                        }
                    }
                    Button {
                        text: qsTr("Delete")
                        Layout.fillHeight: true
                        implicitWidth: height
                        anchors {
                            verticalCenter: parent.verticalCenter
                            margins: 0
                        }
                        contentItem:  Image {
                            fillMode: Image.PreserveAspectFit
                            source: "../images/delete.png"
                        }
                        onClicked: {
                            var serverObject = {"host":model.host, "port":model.port}
                            if (model.last_connect != undefined && model.last_connect != "") {
                                serverObject.last_connect = model.last_connect
                            }

                            if (index === indexConnectedServer) {
                                mainWindow.closeServerConnection()
                            }

                            mainWindow.config.deleteRow(mainWindow.serverTable, serverObject)                            
                            mainWindow.serverModel.remove(index)
                        }
                    }
                    Switch {
                        //                    id: checkConnection
                        //bug on Android when use Switch with ButtonGroup - Switch do not off
                        //                    ButtonGroup.group: savedServersButtonGroup
                        checked: index == indexConnectedServer
                        onClicked: {
                            if (checked) {
                                if (indexConnectedServer >= 0) {
                                    mainWindow.closeServerConnection()
                                }

                                mainWindow.connectToServer(index)
                                indexConnectedServer = index
                            } else {
                                if (indexConnectedServer >= 0) {
                                    mainWindow.closeServerConnection()
                                }
                                indexConnectedServer = -1
                            }
                        }

                        //                    Component.onCompleted: {
                        //                        if (Qt.platform.os == "android") {
                        //                            antiCheked = !checked
                        //                        }
                        //                    }
                    }
                }
            }
            background: Rectangle {
                implicitWidth: control.width
                //                opacity: enabled ? 1 : 0.3
                color: !control.down ? "#eeeeee" : "#dddedf"

                Rectangle {
                    width: parent.width
                    height: 1
                    color: !control.down ? "white" : Material.accent
                    //                    color: "white"
                    anchors.bottom: parent.bottom
                }
            }
        }
    }

    SettingsForm {
        id: settingsForm
        spinBoxPort.value: mainWindow.defaultPort
        spinBoxPort.onValueChanged: {
            mainWindow.defaultPort = spinBoxPort.value
        }

        listSavedServers.implicitHeight: Math.max(listSavedServers.contentHeight, minHeight)
        listSavedServers.model: mainWindow.serverModel
        listSavedServers.delegate: serverDelegate

        listAvailabelServers.implicitHeight: Math.max(listAvailabelServers.contentHeight, minHeight)
        listAvailabelServers.model: ListModel {
            id: availabelServersModel
        }
        listAvailabelServers.delegate: ItemDelegate {
            id: availabelServersDelegate
            spacing: mainWindow.mainMargin
            width: flickable.width - 20
            height: buttonHeight
            contentItem: RowLayout {
                id: testRowLayout
                spacing: parent.spacing
                anchors {
                    fill: parent
                    margins: 0
//                    topMargin: 0
//                    bottomMargin: 0
//                    leftMargin: mainWindow.mainMargin
//                    rightMargin: 2 * mainWindow.mainMargin
//                    verticalCenter: parent.verticalCenter
                }
                Text {
                    text: model.server + ":" + model.port.toString()
                    font: availabelServersDelegate.font
                    opacity: !availabelServersDelegate.down ? 0.87 : 1
                    color: "black"
                    Layout.fillHeight: true
                    verticalAlignment: Qt.AlignVCenter
                    leftPadding: mainWindow.mainMargin
                    anchors {
                        verticalCenter: parent.verticalCenter
                        margins: 0
                    }
                }

                Button {
                    id: addAvailabelServerButton
                    text: "+"
                    implicitWidth: implicitHeight
                    topPadding: 0
                    bottomPadding: 0
                    anchors {
                        verticalCenter: parent.verticalCenter
                        margins: 0
                        rightMargin: mainWindow.mainMargin * 2
                        right: parent.right
                    }
                    contentItem:  Image {
                        fillMode: Image.Pad
                        source: "../images/plus-black.png"
                        sourceSize.height: availabelServersDelegate.height
                    }
                    onClicked: {
                        var serverObject = {"host":model.server, "port":model.port}
                        mainWindow.config.addRow(mainWindow.serverTable, serverObject)
                        mainWindow.serverModel.append(serverObject)
                    }
                }
            }
            background: Rectangle {
                implicitHeight: availabelServersDelegate.height
                implicitWidth: availabelServersDelegate.width
                color: !availabelServersDelegate.down ? "#eeeeee" : "#dddedf"

                Rectangle {
                    width: parent.width
                    height: 1
                    color: !availabelServersDelegate.down ? "white" : Material.accent
                    //                    color: "white"
                    anchors.bottom: parent.bottom
                }
            }
        }        
    }

    DiscoverServer {
        id: discoverServer
        onServerDiscovered: {
            availabelServersModel.append({"server":serverAddr, "port":serverPort/*.toString()*/})
        }
    }

    Popup {
        id: serverEdit
        property bool addServerWindow: true
        property int modelIndex: -1
        x: (mainWindow.width - width) / 2
        y: (mainWindow.height - height) / 3
        width: Math.min(parent.width, parent.height) / 3 * 2
        height: settingsColumn.implicitHeight + topPadding + bottomPadding
        modal: true
        focus: true
        contentItem: ServerEdit {
            id: settingsColumn

            buttonOk.onClicked: {
                if (serverEdit.addServerWindow) {
                    console.log("addServer")
                    var serverObject = {"host":serverName.text, "port":serverPort.value, "passphrase":serverPassphrase.text}
                    mainWindow.config.addRow(mainWindow.serverTable, serverObject)
                    mainWindow.serverModel.append(serverObject)
                } else {
                    console.log("editServer")
                    mainWindow.config.updateRow(mainWindow.serverTable, {"host":serverName.text, "port":serverPort.value, "passphrase":serverPassphrase.text},
                                                mainWindow.serverModel.objectWithRoles(serverEdit.modelIndex))
                    mainWindow.serverModel.setProperty(serverEdit.modelIndex, "host", serverName.text)
                    mainWindow.serverModel.setProperty(serverEdit.modelIndex, "port", serverPort.value)
                    mainWindow.serverModel.setProperty(serverEdit.modelIndex, "passphrase", serverPassphrase.text)
                    serverEdit.addServerWindow = true
                }

                serverEdit.close()
            }

            buttonCancel.onClicked: {
                serverEdit.close()
            }
        }

    }

    Button {
        id: flowButton
        text: "+"
        font.pointSize: 22
        x: mainWindow.width - width - buttonHeight / 3 * 2
        y: mainWindow.height - mainWindow.header.height - height - buttonHeight / 3 * 2
        width: Math.max(buttonHeight, 50)
        height: width
        contentItem: Image {
            fillMode: Image.Pad
            source: "../images/plus.png"
            sourceSize.height: parent.height
        }
        background: Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: !flowButton.down ? Material.primary : Qt.darker(Material.primary)
//                    color: Material.primary
//                    border.color: "#26282a"
//                    border.width: 1
                    radius: implicitWidth / 2
                }
        z: 10
        onClicked: addServer()
    }

//    Rectangle {
//        color: "white"
//        z: 10
//        width: testText.width
//        height: testText.height
//    Text {
//        id: testText
////        text: "discoverServer"
////        z: 10

//    }
//    }

    ScrollIndicator.vertical: ScrollIndicator {}

    function addServer(srvName, srvPort, srvPsk) {
//        console.log("mainWindow.width", mainWindow.width, "mainWindow.height", mainWindow.height)
        settingsColumn.serverName.text = srvName || ""
        settingsColumn.serverPort.value = srvPort || settingsForm.spinBoxPort.value
        settingsColumn.serverPassphrase.text = srvPsk || ""
        serverEdit.open()
    }

    Component.onCompleted: {
        indexConnectedServer = mainWindow.lastConnectModelIndex
        discoverServer.scanLocalnet()
    }

}
