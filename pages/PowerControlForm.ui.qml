import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

Flickable {
    id: pane1
    property alias buttonRestart: buttonRestart
    property alias buttonPowerOff: buttonPowerOff
    property alias buttonLogOff: buttonLogOff
    property alias buttonLock: buttonLock
    property alias buttonSleep: buttonSleep
    property alias buttonHibernate: buttonHibernate
    property alias buttonAbortTimer: buttonAbortTimer
    property alias buttonTimer: buttonTimer
    property alias columns: gridLayout.columns
    property alias gridLayout: gridLayout

    property color  backgroundColor/*: "#3949ab"*/

    GridLayout {
        id: gridLayout
        columns: 2
        rowSpacing: 0
        columnSpacing: 5
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: columnSpacing
            rightMargin: columnSpacing
//            topMargin: 12
//            bottomMargin: 12
        }

        Button {
            id: buttonRestart
            text: qsTr("Перезагрузить")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonPowerOff
            text: qsTr("Выключить")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonLogOff
            text: qsTr("Выйти\nиз системы")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonLock
            text: qsTr("Заблокировать")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonSleep
            text: qsTr("Сон")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonHibernate
            text: qsTr("Гибернация")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonTimer
            text: qsTr("Таймер")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

        Button {
            id: buttonAbortTimer
            text: qsTr("Отключить\nтаймер")
            Material.background: backgroundColor
//            Layout.fillWidth: true
//            Layout.fillHeight: true
        }

    }
}
