import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

Flickable {
    id: pane1
    //    width: 400
    //    height: 400
    property alias gridLayoutMedia: gridLayoutMedia
    property alias columns: gridLayoutMedia.columns
    property alias buttonVolMute: buttonVolMute
    property alias buttonVolDown: buttonVolDown
    property alias buttonVolUp: buttonVolUp
    property alias buttonMediaPlay: buttonMediaPlay
    property alias buttonMediaStop: buttonMediaStop
    property alias buttonMediaPrevious: buttonMediaPrevious
    property alias buttonMediaNext: buttonMediaNext
    property alias buttonMediaPlayPause: buttonMediaPlayPause

    property color backgroundColor/*: "#303f9f"*/

    GridLayout {
        id: gridLayoutMedia
        columns: 2
        rowSpacing: 0
        columnSpacing: 5
        anchors.left: parent.left
        anchors.leftMargin: columnSpacing
        anchors.right: parent.right
        anchors.rightMargin: columnSpacing

        Button {
            id: buttonVolDown
            text: qsTr("VolDown")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonVolUp
            text: qsTr("VolUp")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonVolMute
            text: qsTr("Mute")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonMediaPlayPause
            text: qsTr("Play/Pause")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonMediaPlay
            text: qsTr("Play")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonMediaStop
            text: qsTr("Stop")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonMediaPrevious
            text: qsTr("Previous")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

        Button {
            id: buttonMediaNext
            text: qsTr("Next")
            Material.background: backgroundColor
            Layout.fillWidth: true
        }

    }    
}
