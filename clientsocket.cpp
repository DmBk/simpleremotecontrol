#include "ClientSocket.h"

#include <QDebug>
#include <QTime>

ClientSocket::ClientSocket(QObject *parent) : QSslSocket(parent),
    m_dataStreamVersion(QDataStream::Qt_5_7), m_maxDataStreamVersion(QDataStream::Qt_5_7),
    m_clientState(ControlState::NotReady)
{    
    //    connect(this, &QAbstractSocket::connected, this, &ClientSocket::onConnected);
}

ClientSocket::ClientSocket(qintptr socketDescriptor, QObject *parent) : QSslSocket(parent),
    m_dataStreamVersion(QDataStream::Qt_5_7), m_maxDataStreamVersion(QDataStream::Qt_5_7),
    m_clientState(ControlState::NotReady)
{
    this->setSocketDescriptor(socketDescriptor);
}

ClientSocket::~ClientSocket()
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
}

QDataStream::Version ClientSocket::dataStreamVersion() const
{
    return m_dataStreamVersion;
}

QDataStream::Version ClientSocket::maxDataStreamVersion() const
{
    return m_maxDataStreamVersion;
}

void ClientSocket::setDataStreamVersion(const QDataStream::Version ver)
{
    m_dataStreamVersion = ver;
}

void ClientSocket::setReadYourself(bool readYourself)
{
    disconnect(this, &QAbstractSocket::readyRead, this, &ClientSocket::readFromSocket);
    if (readYourself)
        connect(this, &QAbstractSocket::readyRead, this, &ClientSocket::readFromSocket);
}

void ClientSocket::readFromSocket()
{
    QDataStream in;
    in.setDevice(this);
    in.setVersion(m_dataStreamVersion);

    qDebug() << bytesAvailable() << "bytes available";
    in.startTransaction();

    VectorInt32 vector;
    QByteArray data;
    in >> vector >> data;

    if (!in.commitTransaction())
        return;

//    qDebug() << "data:" << data << sizeof(data[1]) << sizeof(data) << data.size();
    emit dataReaded(vector, data);

    if (bytesAvailable() > 0)
        readFromSocket();
}

void ClientSocket::writeToSocket(const QByteArray &data)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(m_dataStreamVersion);
    out << data;
    this->write(block);
}

void ClientSocket::writeToSocket(const VectorInt32 &vector, const QByteArray &data)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(m_dataStreamVersion);
    out << vector << data;
    this->write(block);
}

void ClientSocket::setClientState(const ControlState &state)
{
    if (state != m_clientState) {
        m_clientState = state;
        emit clientStateChanged(m_clientState);
    }
}

ControlState ClientSocket::clientState() const
{
    return m_clientState;
}

