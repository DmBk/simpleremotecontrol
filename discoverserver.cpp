#include "discoverserver.h"
#include <QDebug>

DiscoverServer::DiscoverServer(QObject *parent) : QObject(parent)
{
    m_udpSocket.bind(QHostAddress::AnyIPv4);
    connect(&m_udpSocket, &QAbstractSocket::readyRead, this, &DiscoverServer::onUdpReadyRead);
}

QList<QNetworkAddressEntry> DiscoverServer::addressEntries() const
{
    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    QList<QNetworkAddressEntry> addressEntries;
    for (int i = 0; i < interfaces.size(); ++i) {
        addressEntries.append(interfaces.at(i).addressEntries());
    }

    return addressEntries;
}

void DiscoverServer::scanLocalnet()
{
    QList<QNetworkAddressEntry> listAddressEntry {addressEntries()};
    for (int i = 0; i < listAddressEntry.length(); ++i) {
        if (ipInLocalSubnet(listAddressEntry.at(i).ip())) {
            broadcastScan(listAddressEntry.at(i));
        }
    }
}

bool DiscoverServer::ipInLocalSubnet(QHostAddress ipAddress) const
{
    QVector<QPair<QHostAddress, int> > localSubnets {qMakePair(QHostAddress("10.0.0.0"), 8),
                                                     qMakePair(QHostAddress("172.16.0.0"), 12),
                                                     qMakePair(QHostAddress("192.168.0.0"), 16)};

    for (int i = 0; i < localSubnets.length(); ++i) {
        if (ipAddress.isInSubnet(localSubnets.at(i)))
            return true;
    }

    return false;
}

void DiscoverServer::broadcastScan(QNetworkAddressEntry addressEntry, quint16 port)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out << Command::ServerDiscover;
    m_udpSocket.writeDatagram(block, addressEntry.broadcast(), port);
}

void DiscoverServer::onUdpReadyRead()
{
    while (m_udpSocket.hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(m_udpSocket.pendingDatagramSize());
            QHostAddress senderAddress;
            quint16 senderPort;

            m_udpSocket.readDatagram(datagram.data(), datagram.size(),
                                    &senderAddress, &senderPort);
//            qDebug() << datagram << datagram.size() << senderAddress.toString() << QHostAddress(senderAddress.toIPv4Address()).toString() <<senderPort;
            QDataStream in(datagram);
            qint32 reply;
            in >> reply;
            if (reply == static_cast<qint32>(Command::ServerOffer)) {
                //QHostAddress.toString() return IPv6:IPv4 address if IPv6 enable on server
                emit serverDiscovered(QHostAddress(senderAddress.toIPv4Address()).toString(), static_cast<int>(senderPort));
            }
        }
}

//void DiscoverServer::scanSubnet(quint32 beginIp, quint32 endIp)
//{
//    if (beginIp > endIp)
//        return;

////    for (quint32 i = beginIp; i < )
//}

//QPair<quint32, quint32> DiscoverServer::rangeLocalIp() const
//{
//   QNetworkConfigurationManager netConfigManager;
//   QList<QNetworkConfiguration> allNetConfigList =
//           netConfigManager.allConfigurations(QNetworkConfiguration::Active);
//   QList<QNetworkConfiguration> netConfigList;
//   for (int i = 0; i < allNetConfigList.size(); ++i) {
//       if (allNetConfigList.at(i).bearerType() == QNetworkConfiguration::BearerWLAN) {
//           netConfigList << allNetConfigList.at(i);
//       }
//   }

//}
